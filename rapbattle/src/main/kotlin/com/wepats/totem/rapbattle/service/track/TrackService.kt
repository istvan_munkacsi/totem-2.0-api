package com.wepats.totem.rapbattle.service.track

import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.model.track.Track
import com.wepats.totem.rapbattle.model.track.TrackDto
import com.wepats.totem.rapbattle.model.track.TrackUpdate
import com.wepats.totem.rapbattle.repository.TrackRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*

@Service
class TrackService : TrackServiceContract {

    @Autowired
    private lateinit var repo: TrackRepository

    override fun getTrackById(id: Int): Track? {
        return repo.getById(id)
    }

    override fun getTrackInNotFinishedBattleById(id: Int): Optional<Int> {
        val minBattleStartedAt = System.currentTimeMillis() - TrackBattle.BATTLE_DURATION_MS
        return repo.getTrackInNotFinishedBattleById(id, minBattleStartedAt)
    }

    override fun getTracksByUserId(userId: String): List<Track> {
        return repo.getTracksByUserId(userId)
    }

    override fun getDefaultPlaylist(): List<Track> {
        return repo.getDefaultPlaylist()
    }

    override fun addTrack(userId: String, track: Track): Track? {
        val userTracksCount = repo.getUserTracksCount(userId)
        return if(userTracksCount < TrackDto.MAX_TRACKS_COUNT_FOR_USER) {
            repo.insert(userId, track.title, track.mediaSource, track.duration, track.trackUUID)
            repo.getLastInserted()
        } else {
            println("userTracksCount: $userTracksCount")
            null
        }
    }

    override fun removeTrack(trackId: Int): Int {
        return repo.updateToNull(trackId)
    }

    override fun update(track: TrackUpdate): Int {
        return if (track.coverChanged() && track.titleChanged()) {
            repo.update(track.id, track.title!!, track.coverURL, track.coverUUID)
        } else if (track.coverChanged()) {
            repo.updateCover(track.id, track.coverURL, track.coverUUID)
        } else if (track.titleChanged()) {
            repo.updateTitle(track.id, track.title!!)
        } else 0
    }
}