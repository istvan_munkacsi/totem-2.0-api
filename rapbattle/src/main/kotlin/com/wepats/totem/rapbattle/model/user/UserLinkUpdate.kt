package com.wepats.totem.rapbattle.model.user

data class UserLinkUpdate(
        val userId: String,
        val link: String?
)