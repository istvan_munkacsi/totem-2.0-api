package com.wepats.totem.rapbattle.controller

import com.wepats.totem.rapbattle.model.track.Track
import com.wepats.totem.rapbattle.model.track.TrackUpdate
import com.wepats.totem.rapbattle.service.track.TrackServiceContract
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/totem/api/track")
class TrackController {

    companion object {
        const val ID_PARAM = "trackId"
    }

    @Autowired
    lateinit var service: TrackServiceContract

    @GetMapping("/getDefaultPlaylist")
    fun getDefaultPlaylist(): List<Track> {
        return service.getDefaultPlaylist()
    }

    @GetMapping("/getAllForUser/{${UserController.ID_PARAM}}")
    fun getTracksForUser(@PathVariable(UserController.ID_PARAM) userId: String): List<Track> {
        return service.getTracksByUserId(userId)
    }

    @GetMapping("/{$ID_PARAM}")
    fun getTrackById(@PathVariable(ID_PARAM) trackId: Int): Track? {
        return service.getTrackById(trackId)
    }

    @DeleteMapping("/delete/{$ID_PARAM}")
    fun deleteTrackById(@PathVariable(ID_PARAM) trackId: Int): ResponseEntity<Any> {
        val activeBattleWhereTrackTakesPart = service.getTrackInNotFinishedBattleById(trackId)
        return if (activeBattleWhereTrackTakesPart.isPresent) {
            ResponseEntity(HttpStatus.CONFLICT)
        } else {
            ResponseEntity.ok(service.removeTrack(trackId))
        }
    }

    @PostMapping("/addTrack/{${UserController.ID_PARAM}}")
    fun addTrack(@PathVariable(UserController.ID_PARAM) userId: String, @RequestBody track: Track): ResponseEntity<Track> {
        val insertedTrack = service.addTrack(userId, track)
        return if (insertedTrack != null) {
            ResponseEntity.ok(insertedTrack)
        } else {
            ResponseEntity.noContent().build()
        }
    }

    @PutMapping("/updateTrack")
    fun updateTrack(@RequestBody track: TrackUpdate): Int {
        return service.update(track)
    }
}