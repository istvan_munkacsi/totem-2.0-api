package com.wepats.totem.rapbattle.error_response

import com.google.gson.Gson

object CustomErrors {

    private val gson = Gson()

    private const val TRACK_TAKES_PART_IN_NOT_FINISHED_BATTLE_ERROR_CODE = 9000
    private const val TRACK_TAKES_PART_IN_NOT_FINISHED_BATTLE_MESSAGE = "Track takes part in not finished battle"

    private val trackTakesPartInNotFinishedBattleErrorResponse = ErrorResponseDto(TRACK_TAKES_PART_IN_NOT_FINISHED_BATTLE_ERROR_CODE, TRACK_TAKES_PART_IN_NOT_FINISHED_BATTLE_MESSAGE)
    val trackTakesPartInNotFinishedBattleErrorResponseJson = gson.toJson(trackTakesPartInNotFinishedBattleErrorResponse)
}