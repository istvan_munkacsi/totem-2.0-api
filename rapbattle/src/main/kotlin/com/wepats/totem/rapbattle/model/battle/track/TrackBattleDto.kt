package com.wepats.totem.rapbattle.model.battle.track

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto
import com.wepats.totem.rapbattle.model.battle.BattleInfo
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto.Companion.TABLE
import javax.persistence.*

@Entity
@Table(name = TABLE)
data class TrackBattleDto(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int,

        @Column(name = STARTED_AT_COLUMN, columnDefinition = "VARCHAR(13) DEFAULT '0'") val startedAt: String,

        @OneToMany(mappedBy = "battleDto")
        val applications: List<TrackBattleApplicationDto>
) {
    companion object {
        const val TABLE = "track_battle"
        const val OBJECT_NAME = "TrackBattleDto"

        const val ID_COLUMN = "id"
        const val STARTED_AT_COLUMN = "started_at"

        fun getBattleStatus(applicationsCount: Int, startedAt: Long): BattleInfo.BattleStatus {
            return when {
                applicationsCount == 1 -> BattleInfo.BattleStatus.SEARCHING
                TrackBattle.BATTLE_DURATION_MS - (System.currentTimeMillis() - startedAt) > 0 -> BattleInfo.BattleStatus.LASTS
                else -> BattleInfo.BattleStatus.FINISHED
            }
        }
    }
}