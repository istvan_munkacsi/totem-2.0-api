package com.wepats.totem.rapbattle.model.battle

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationBaseInfo
import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto

data class BattleInfo(
        val battleId: Int,
        val status: BattleStatus,
        val info: MutableList<TrackBattleApplicationBaseInfo>
) {

    companion object {
        const val PATH = "com.wepats.totem.rapbattle.model.battle.BattleInfo"
    }

    constructor(battleId: Int, applicationsCount: Int, startedAt: String)
            : this(battleId, TrackBattleDto.getBattleStatus(applicationsCount, startedAt.toLong()), mutableListOf())

    enum class BattleStatus {
        SEARCHING, LASTS, FINISHED
    }
}