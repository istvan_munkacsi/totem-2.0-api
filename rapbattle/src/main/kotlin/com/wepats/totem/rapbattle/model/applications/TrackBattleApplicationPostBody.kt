package com.wepats.totem.rapbattle.model.applications

data class TrackBattleApplicationPostBody(
        val trackId: Int,
        val bet: Int)