package com.wepats.totem.rapbattle.service.user

import com.wepats.totem.rapbattle.model.user.User
import com.wepats.totem.rapbattle.model.user.UserLinkUpdate
import com.wepats.totem.rapbattle.model.user.UserUpdate

interface UserServiceContract {

    fun getOrSave(userId: String): User?
    fun getCoins(userId: String): Int
    fun updateCoinsCount(userId: String, newCoinsCount: Int): Int
    fun plusCoinsCount(userId: String, plusCoinsCount: Int): Int
    fun update(userUpdate: UserUpdate): Int
    fun updateUserLink(userLinkUpdate: UserLinkUpdate): Int
}