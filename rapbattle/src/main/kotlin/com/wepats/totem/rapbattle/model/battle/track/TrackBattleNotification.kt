package com.wepats.totem.rapbattle.model.battle.track

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationNotification

data class TrackBattleNotification(
        val battleId: Int,
        val bet: Int,
        var finishesIn: Long,
        var applications: List<TrackBattleApplicationNotification>
)