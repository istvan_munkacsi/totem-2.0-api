package com.wepats.totem.rapbattle.repository.battle

import com.wepats.totem.rapbattle.model.vote.TrackBattleApplicationUserComposeId
import com.wepats.totem.rapbattle.model.vote.TrackBattleVoteDto
import com.wepats.totem.rapbattle.model.vote.TrackBattleVoteDto.Companion.COMMENT_COLUMN
import com.wepats.totem.rapbattle.model.vote.TrackBattleVoteDto.Companion.RATING_COLUMN
import com.wepats.totem.rapbattle.model.vote.TrackBattleVoteDto.Companion.TABLE
import com.wepats.totem.rapbattle.model.vote.VoteDetails
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface TrackBattleVoteRepository : CrudRepository<TrackBattleVoteDto, TrackBattleApplicationUserComposeId> {

    @Transactional
    @Modifying
    @Query("INSERT INTO $TABLE" +
            "(" +
            "   ${TrackBattleApplicationUserComposeId.COLUMN_NAME_USER_ID}, " +
            "   ${TrackBattleApplicationUserComposeId.COLUMN_NAME_TRACK_BATTLE_APPLICATION_ID}, " +
            "   $COMMENT_COLUMN" +
            ") " +
            "VALUES(:userId, :trackBattleApplicationId, \"\")", nativeQuery = true)
    fun insert(trackBattleApplicationId: Int, userId: String)

    @Transactional
    @Modifying
    @Query("UPDATE $TABLE SET $COMMENT_COLUMN = CASE WHEN (:comment = null) THEN \"\" ELSE :comment END, " +
            "$RATING_COLUMN = CASE WHEN (:rating = null) THEN 0.0 ELSE :rating END " +
            "WHERE ${TrackBattleApplicationUserComposeId.COLUMN_NAME_USER_ID} = :userId " +
            "AND ${TrackBattleApplicationUserComposeId.COLUMN_NAME_TRACK_BATTLE_APPLICATION_ID} = :trackBattleApplicationId"
            , nativeQuery = true)
    fun update(trackBattleApplicationId: Int, userId: String, comment: String?, rating: Float?)

    @Query("SELECT new ${VoteDetails.PATH}(user.id, user.nickname, user.photoURL, vote.rating, vote.comment) FROM ${TrackBattleVoteDto.OBJECT} vote INNER JOIN vote.userDto user WHERE vote.votedForTrackBattleApplication.id = :applicationId")
    fun getVotesWithDetails(applicationId: Int, pageable: Pageable): List<VoteDetails>
}