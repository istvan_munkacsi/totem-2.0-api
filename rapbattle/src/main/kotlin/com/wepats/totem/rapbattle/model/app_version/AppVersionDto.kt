package com.wepats.totem.rapbattle.model.app_version

import com.wepats.totem.rapbattle.model.app_version.AppVersionDto.Companion.TABLE
import javax.persistence.*

@Entity
@Table(name = TABLE)
data class AppVersionDto(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int,
        val version: Float,
        val mustUpdate: Int
) {

    enum class CompareResponse {
        ACTUAL, NOT_ACTUAL, MUST_UPDATE
    }

    companion object {
        const val TABLE = "android_app_version"
    }
}