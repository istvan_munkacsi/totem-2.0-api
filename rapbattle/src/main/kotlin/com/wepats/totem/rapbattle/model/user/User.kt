package com.wepats.totem.rapbattle.model.user

data class User(
        val id: String,
        val nickname: String,
        val coinsCount: Int,
        val photoURL: String? = null,
        val photoUUID: String? = null,
        val link: String? = null
) {
    constructor(id: String) : this(id, DEFAULT_NICKNAME, DEFAULT_COINS_COUNT)

    companion object {
        const val PATH = "com.wepats.totem.rapbattle.model.user.User"

        const val DEFAULT_NICKNAME = "Player"
        const val DEFAULT_COINS_COUNT = 3000
    }
}