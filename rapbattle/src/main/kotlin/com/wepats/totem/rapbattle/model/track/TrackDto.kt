package com.wepats.totem.rapbattle.model.track

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.TABLE
import com.wepats.totem.rapbattle.model.user.UserDto
import javax.persistence.*

@Entity
@Table(name = TABLE)
data class TrackDto(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int,
        @Column(name = TITLE_COLUMN) val title: String?,
        @Column(name = URL_COLUMN) val url: String?,
        @Column(name = DURATION_COLUMN) val duration: Long?,
        @Column(name = COVER_URL_COLUMN) val coverURL: String?,
        @Column(name = COVER_UUID_COLUMN) val coverUUID: String?,
        @Column(name = TRACK_UUID_COLUMN) val trackUUID: String?,
        @Column(name = IS_REMOVED_COLUMN, columnDefinition = "INTEGER DEFAULT 0") val isRemoved: Int,
        @Column(name = IS_IN_DEFAULT_PLAYLIST_COLUMN, columnDefinition = "INTEGER DEFAULT 0") val isInDefaultPlaylist: Int,

        @ManyToOne
        @JoinColumn(name = USER_ID_COLUMN)
        val userDto: UserDto,

        @OneToMany(mappedBy = "trackDto")
        val trackBattleApplications: List<TrackBattleApplicationDto>
) {
    companion object {
        const val TABLE = "track"
        const val OBJECT_NAME = "TrackDto"
        const val ID_COLUMN = "id"

        const val USER_ID_COLUMN = "user_id"
        const val TITLE_COLUMN = "title"
        const val DURATION_COLUMN = "duration"
        const val URL_COLUMN = "url"
        const val COVER_URL_COLUMN = "cover_url"
        const val COVER_UUID_COLUMN = "cover_uuid"
        const val TRACK_UUID_COLUMN = "track_uuid"
        const val IS_REMOVED_COLUMN = "is_removed"
        const val IS_IN_DEFAULT_PLAYLIST_COLUMN = "is_in_default_playlist"

        const val MAX_TRACKS_COUNT_FOR_USER = 10
    }
}