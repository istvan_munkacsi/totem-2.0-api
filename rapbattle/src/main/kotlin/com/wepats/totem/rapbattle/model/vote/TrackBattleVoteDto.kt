package com.wepats.totem.rapbattle.model.vote

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto
import com.wepats.totem.rapbattle.model.user.UserDto
import com.wepats.totem.rapbattle.model.vote.TrackBattleVoteDto.Companion.TABLE
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = TABLE)
data class TrackBattleVoteDto(
        @EmbeddedId
        val battleApplicationUserComposeId: TrackBattleApplicationUserComposeId,

        @ManyToOne @JoinColumn(name = TrackBattleApplicationUserComposeId.COLUMN_NAME_USER_ID, insertable = false, updatable = false)
        val userDto: UserDto,

        @ManyToOne @JoinColumn(name = TrackBattleApplicationUserComposeId.COLUMN_NAME_TRACK_BATTLE_APPLICATION_ID, insertable = false, updatable = false)
        val votedForTrackBattleApplication: TrackBattleApplicationDto,

        @Column(name = RATING_COLUMN, columnDefinition = "FLOAT DEFAULT 0.0")
        val rating: Float,

        @Column(name = COMMENT_COLUMN, columnDefinition = "LONGTEXT")
        val comment: String


) : Serializable {

    companion object {
        const val TABLE = "track_battle_vote"
        const val RATING_COLUMN = "rating"
        const val COMMENT_COLUMN = "comment"
        const val OBJECT = "TrackBattleVoteDto"
    }
}