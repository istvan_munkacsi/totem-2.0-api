package com.wepats.totem.rapbattle.service.track

import com.wepats.totem.rapbattle.model.track.Track
import com.wepats.totem.rapbattle.model.track.TrackUpdate
import java.util.*

interface TrackServiceContract {

    fun getTrackById(id: Int): Track?
    fun getTrackInNotFinishedBattleById(id: Int): Optional<Int>
    fun getTracksByUserId(userId: String): List<Track>
    fun getDefaultPlaylist(): List<Track>
    fun addTrack(userId: String, track: Track): Track?
    fun removeTrack(trackId: Int): Int
    fun update(track: TrackUpdate): Int
}