package com.wepats.totem.rapbattle.service.judge_battle

import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.model.vote.TrackBattleAddComment
import com.wepats.totem.rapbattle.model.vote.TrackBattleVote
import com.wepats.totem.rapbattle.model.vote.VoteDetails
import com.wepats.totem.rapbattle.repository.battle.TrackBattleVoteRepository
import com.wepats.totem.rapbattle.repository.battle.track.TrackBattleApplicationRepository
import com.wepats.totem.rapbattle.repository.battle.track.TrackBattleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service

@Service
class JudgeBattleService : JudgeBattleServiceContract {


    @Autowired
    lateinit var trackBattleRepository: TrackBattleRepository

    @Autowired
    lateinit var trackBattleApplicationRepository: TrackBattleApplicationRepository

    @Autowired
    lateinit var trackBattleVoteRepository: TrackBattleVoteRepository

    override fun getTrackBattle(userId: String): TrackBattle? {
        val offset = 1000 * 5 //5 seconds
        val minStartedAt = System.currentTimeMillis() - TrackBattle.BATTLE_DURATION_MS + offset
        val list = trackBattleRepository.getForJudgement(userId, minStartedAt, PageRequest.of(0, 1))
        return if (list?.isNotEmpty() == true) list[0].apply {
            applications.addAll(trackBattleApplicationRepository.getByBattleId(id))
        } else null
    }

    override fun getTrackBattleByIdIfAllowedForUser(userId: String, battleId: Int): TrackBattle? {
        val battle = trackBattleRepository.getForJudgementIfAllowedForUser(userId, battleId)
        return battle?.apply {
            applications.addAll(trackBattleApplicationRepository.getByBattleId(id))
        }
    }

    override fun trackBattleVote(trackBattleVote: TrackBattleVote) {
        trackBattleVoteRepository.insert(trackBattleVote.applicationId, trackBattleVote.userId)
    }

    override fun trackBattleAddComment(trackBattleAddComment: TrackBattleAddComment) {
        trackBattleAddComment.comment = validateComment(trackBattleAddComment.comment)

        trackBattleVoteRepository.update(trackBattleAddComment.applicationId, trackBattleAddComment.userId, trackBattleAddComment.comment, trackBattleAddComment.rating)
    }

    private fun validateComment(comment: String): String {
        var validatedComment = comment.trim()

        while (validatedComment.contains("  ")) {
            validatedComment = validatedComment.replace("  ", " ")
        }
        while (validatedComment.contains(" \n")) {
            validatedComment = validatedComment.replace(" \n", "\n")
        }
        while (validatedComment.contains("\n\n")) {
            validatedComment = validatedComment.replace("\n\n", "\n")
        }
        return validatedComment
    }

    override fun getVotesWithDetails(applicationId: Int, pageNumber: Int): List<VoteDetails> {
        return trackBattleVoteRepository.getVotesWithDetails(applicationId, PageRequest.of(pageNumber, 5))
    }
}