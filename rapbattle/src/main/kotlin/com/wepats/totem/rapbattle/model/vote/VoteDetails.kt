package com.wepats.totem.rapbattle.model.vote

data class VoteDetails(
        val userId: String,
        val userNickname: String,
        val userPhotoURL: String?,
        val rating: Float,
        val comment: String
) {

    companion object {
        const val PATH = "com.wepats.totem.rapbattle.model.vote.VoteDetails"
    }
}