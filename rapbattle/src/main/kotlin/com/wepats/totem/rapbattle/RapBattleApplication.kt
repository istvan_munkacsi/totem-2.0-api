package com.wepats.totem.rapbattle

import org.quartz.impl.StdSchedulerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.quartz.SpringBeanJobFactory

@SpringBootApplication
@EnableScheduling
class RapBattleApplication {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<RapBattleApplication>(*args)
        }
    }
}

