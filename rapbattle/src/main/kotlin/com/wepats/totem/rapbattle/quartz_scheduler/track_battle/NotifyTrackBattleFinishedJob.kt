package com.wepats.totem.rapbattle.quartz_scheduler.track_battle

import com.google.gson.Gson
import com.wepats.totem.rapbattle.firebase.PushNotificationHelper
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleNotification
import com.wepats.totem.rapbattle.repository.UserRepository
import com.wepats.totem.rapbattle.repository.battle.track.TrackBattleApplicationRepository
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

@Component
class NotifyTrackBattleFinishedJob : Job {

    companion object {
        const val TRACK_BATTLE_NOTIFICATION_KEY = "track_battle_notification"
    }

    @Autowired
    private lateinit var trackBattleApplicationRepository: TrackBattleApplicationRepository

    @Qualifier("userRepository")
    @Autowired
    private lateinit var userRepository: UserRepository

    override fun execute(context: JobExecutionContext?) {
        val trackBattleNotificationJson = context?.jobDetail?.jobDataMap?.get(TRACK_BATTLE_NOTIFICATION_KEY) as String?
        if (trackBattleNotificationJson != null) {
            val trackBattleNotification = Gson().fromJson(trackBattleNotificationJson, TrackBattleNotification::class.java)
            if (trackBattleNotification != null) {
                Thread.sleep(trackBattleNotification.finishesIn)
                refreshScore(trackBattleNotification)
            }
        }
    }

    private fun refreshScore(trackBattleNotification: TrackBattleNotification) {
        val newScores = trackBattleApplicationRepository.getUserIdAndScore(trackBattleNotification.battleId)
        for (newScore in newScores) {
            for (application in trackBattleNotification.applications) {
                if (newScore.userId == application.userId) {
                    application.userScore = newScore.score
                }
            }
        }
        notifyTrackBattleFinished(trackBattleNotification)
    }

    fun notifyTrackBattleFinished(finishedBattle: TrackBattleNotification) {
        val scoreUser1 = finishedBattle.applications[0].userScore
        val scoreUser2 = finishedBattle.applications[1].userScore

        val successResult = 1
        when {
            scoreUser1 > scoreUser2 -> {
                val user1PlusCoins = finishedBattle.bet * 2

                val result = userRepository.updateCoinsCountByBet(user1PlusCoins, finishedBattle.applications[0].userId)
                if (result == successResult) {
                    sendBattleFinishedNotifications(finishedBattle, user1PlusCoins, 0)
                }
            }
            scoreUser2 > scoreUser1 -> {
                val user2PlusCoins = finishedBattle.bet * 2

                val result = userRepository.updateCoinsCountByBet(user2PlusCoins, finishedBattle.applications[1].userId)
                if (result == successResult) {
                    sendBattleFinishedNotifications(finishedBattle, 0, user2PlusCoins)
                }
            }
            scoreUser1 == scoreUser2 -> {
                val user1PlusCoins = finishedBattle.bet
                val user2PlusCoins = finishedBattle.bet

                val result1 = userRepository.updateCoinsCountByBet(user1PlusCoins, finishedBattle.applications[0].userId)
                val result2 = userRepository.updateCoinsCountByBet(user2PlusCoins, finishedBattle.applications[1].userId)
                if (result1 == successResult && result2 == successResult) {
                    sendBattleFinishedNotifications(finishedBattle, user1PlusCoins, user2PlusCoins)
                }
            }
        }
    }

    private fun sendBattleFinishedNotifications(finishedBattle: TrackBattleNotification, user1PlusCoins: Int, user2PlusCoins: Int) {
        val scoreUser1 = finishedBattle.applications[0].userScore
        val scoreUser2 = finishedBattle.applications[1].userScore
        val user1BattleResult = resultStatusForScore(scoreUser1, scoreUser2)
        val user2BattleResult = resultStatusForScore(scoreUser2, scoreUser1)
        val scoreString = "$scoreUser1:$scoreUser2"

        PushNotificationHelper.sendBattleFinishedNotification(
                finishedBattle.applications[0].userId,
                finishedBattle.applications[1].nickname,
                scoreString,
                user1BattleResult,
                user1PlusCoins
        )

        PushNotificationHelper.sendBattleFinishedNotification(
                finishedBattle.applications[1].userId,
                finishedBattle.applications[0].nickname,
                scoreString,
                user2BattleResult,
                user2PlusCoins
        )
    }

    private fun resultStatusForScore(firstScore: Int, secondScore: Int): PushNotificationHelper.BattleResult {
        return when {
            firstScore > secondScore -> PushNotificationHelper.BattleResult.WON
            firstScore == secondScore -> PushNotificationHelper.BattleResult.DRAW
            else -> PushNotificationHelper.BattleResult.LOST
        }
    }
}