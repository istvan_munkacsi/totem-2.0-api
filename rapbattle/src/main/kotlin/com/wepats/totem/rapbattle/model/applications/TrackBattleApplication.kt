package com.wepats.totem.rapbattle.model.applications

data class TrackBattleApplication(
        val id: Int,
        val userId: String,
        val nickname: String,
        val trackId: Int?,
        val trackURL: String?,
        val trackTitle: String?,
        val trackDuration: Long?,
        val trackCoverURL: String?,
        val userPhotoURL: String?,
        val userScore: Int?
) {

    companion object {
        const val PATH = "com.wepats.totem.rapbattle.model.applications.TrackBattleApplication"

        fun toTrackBattleApplicationNotification(trackBattleApplication: TrackBattleApplication): TrackBattleApplicationNotification {
            return TrackBattleApplicationNotification(
                    trackBattleApplication.userId,
                    trackBattleApplication.nickname,
                    trackBattleApplication.userScore!!
            )
        }

        fun toTrackBattleApplicationNotification(trackBattleApplications: List<TrackBattleApplication>): List<TrackBattleApplicationNotification> {
            val notifications = mutableListOf<TrackBattleApplicationNotification>()
            for (application in trackBattleApplications) {
                notifications.add(toTrackBattleApplicationNotification(application))
            }

            return notifications
        }
    }
}