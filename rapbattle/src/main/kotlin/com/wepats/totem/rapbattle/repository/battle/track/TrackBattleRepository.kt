package com.wepats.totem.rapbattle.repository.battle.track

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto.Companion.BATTLE_ID_COLUMN
import com.wepats.totem.rapbattle.model.battle.BattleInfo
import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.model.battle.track.TrackBattle.Companion.PATH
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto.Companion.ID_COLUMN
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto.Companion.OBJECT_NAME
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto.Companion.STARTED_AT_COLUMN
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto.Companion.TABLE
import com.wepats.totem.rapbattle.model.track.TrackDto
import com.wepats.totem.rapbattle.model.user.UserDto
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Lock
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.LockModeType
import javax.transaction.Transactional

@Repository
interface TrackBattleRepository : CrudRepository<TrackBattleDto, Int> {

    companion object {
        private const val JOIN_APPLICATIONS = "INNER JOIN battle.applications applications "
        private const val JOIN_TRACK_DTO = "INNER JOIN applications.trackDto track "
        private const val JOIN_USER_DTO = "INNER JOIN track.userDto user "

        private const val JOIN_APPLICATIONS_SQL = "INNER JOIN ${TrackBattleApplicationDto.TABLE} AS applications ON applications.$BATTLE_ID_COLUMN = battle.$ID_COLUMN "
        private const val JOIN_TRACK_SQL = "INNER JOIN ${TrackDto.TABLE} AS track_ ON track_.${TrackDto.ID_COLUMN} = applications.${TrackBattleApplicationDto.TRACK_ID_COLUMN} "
        private const val JOIN_USER_SQL = "INNER JOIN ${UserDto.TABLE} user_ ON user_.${UserDto.ID_COLUMN} = track_.${TrackDto.USER_ID_COLUMN} "

        private const val SELECT_QUERY = "SELECT new $PATH" +
                "(battle.id, battle.startedAt) " +
                "FROM $OBJECT_NAME battle "
    }

    @Query(SELECT_QUERY +
            "WHERE battle.id = :battleId")
    fun getById(battleId: Int): TrackBattle

    @Query("SELECT new ${BattleInfo.PATH}" +
            "(" +
            "   battle.id, " +
            "   SIZE(applications), " +
            "   battle.startedAt" +
            ") FROM $OBJECT_NAME battle " +
            "INNER JOIN battle.applications applications " +
            "WHERE applications.trackDto.userDto.id = :userId " +
            "ORDER BY battle.id DESC"
    )
    fun getByUserId(userId: String, pageable: Pageable): List<BattleInfo>

    @Query(SELECT_QUERY +
            JOIN_APPLICATIONS +
            "WHERE NOT EXISTS(" +
            "   SELECT 1 FROM $OBJECT_NAME subBattle" +
            "   INNER JOIN subBattle.applications subApplications " +
            "   INNER JOIN subApplications.trackDto.userDto subUser " +
            "   LEFT JOIN subApplications.trackBattleVoteDto subVotes " +
            "   WHERE subBattle.id = battle.id " +
            "   AND (subUser.id = :userId OR subVotes.userDto.id = :userId)" +
            ") " +
            "AND CAST(battle.startedAt AS long) > :minStartedAt " +
            "GROUP BY applications.battleDto.id HAVING COUNT(applications.battleDto.id) = 2")
    fun getForJudgement(userId: String, minStartedAt: Long, pageable: Pageable): List<TrackBattle>?

    @Query(SELECT_QUERY +
            JOIN_APPLICATIONS +
            "WHERE NOT EXISTS(" +
            "   SELECT 1 FROM $OBJECT_NAME subBattle" +
            "   INNER JOIN subBattle.applications subApplications " +
            "   INNER JOIN subApplications.trackDto.userDto subUser " +
            "   LEFT JOIN subApplications.trackBattleVoteDto subVotes " +
            "   WHERE subBattle.id = battle.id " +
            "   AND (subUser.id = :userId OR subVotes.userDto.id = :userId)" +
            ") " +
            "AND battle.id = :battleId " +
            "GROUP BY applications.battleDto.id HAVING COUNT(applications.battleDto.id) = 2")
    fun getForJudgementIfAllowedForUser(userId: String, battleId: Int): TrackBattle?

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query(value = "SELECT battle.id FROM $OBJECT_NAME battle " +
            JOIN_APPLICATIONS +
            JOIN_TRACK_DTO +
            JOIN_USER_DTO +
            "WHERE NOT EXISTS (" +
            "   SELECT 1 FROM  $OBJECT_NAME subBattle " +
            "   INNER JOIN subBattle.applications subApplications " +
            "   INNER JOIN subApplications.trackDto subTracks " +
            "   INNER JOIN subTracks.userDto subUsers " +
            "   INNER JOIN subUsers.tracks subUsersTracks " +
            "   WHERE subBattle.id = battle.id " +
            "   AND subUsersTracks.id = :trackId" +
            ") " +
            "GROUP BY applications.battleDto.id HAVING COUNT(applications.battleDto.id) = 1 ")
    fun getFreeBattleId(trackId: Int, pageable: Pageable): List<Int>

    @Modifying
    @Transactional
    @Query(value = "UPDATE $TABLE SET $STARTED_AT_COLUMN = :startedAt WHERE $ID_COLUMN = :battleId", nativeQuery = true)
    fun updateStartedAt(battleId: Int, startedAt: String)

    @Modifying
    @Transactional
    @Query("INSERT INTO $TABLE() VALUES()", nativeQuery = true)
    fun insertNew(): Int
}