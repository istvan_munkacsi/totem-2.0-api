package com.wepats.totem.rapbattle.firebase

import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

object PushNotificationHelper {
    private const val FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send"
    private const val FIREBASE_SERVER_KEY = "AAAATNLnH5o:APA91bG6NFjI_tZTenmNjOAL21Vl-Yjpti_9C7kZY9sof5_0nHbF0WIZbO1azrRbrfzl016GAHh-pSvZWrwkLwgbNsTcjParDmumapwW8NLbXqMzoEnmxNLYfzlaRXzZskfqE1YhYB8S"

    private const val TRACK_BATTLE_FOUND_TOPIC = "track_battle_found_topic"
    private const val TRACK_BATTLE_FINISHED_TOPIC = "track_battle_finished_topic"
    private const val JUDGE_BATTLE_FOUND_TOPIC = "judge_battle_found_topic"

    private const val BATTLE_ID_ARG = "battleId"
    private const val BATTLE_OPPONENT_NAME_ARG = "battleOpponentName"
    private const val BATTLE_SCORE_ARG = "battleScore"
    private const val BATTLE_RESULT_ARG = "battleResult"
    private const val PLUS_COINS_ARG = "plusCoins"

    @Throws(IOException::class)
    fun sendPushNotification(userId: String = "", topic: Topic, body: Map<String, Any>) {
        val url = URL(FIREBASE_API_URL)
        val conn = url.openConnection() as HttpURLConnection
        conn.useCaches = false
        conn.doOutput = true
        conn.requestMethod = "POST"
        conn.setRequestProperty("Authorization", "key=$FIREBASE_SERVER_KEY")
        conn.setRequestProperty("Content-Type", "application/json")

        val json = JSONObject()
        var toTopic = "/topics/${topic.getValue()}"
        if (userId != "") {
            toTopic = "${toTopic}_${userId}"
        }
        json.put("to", toTopic)

        val data = JSONObject()
        for (item in body) {
            data.put(item.key, item.value)
        }
        json.put("data", data)

        val wr = OutputStreamWriter(conn.outputStream, "UTF-8")
        wr.write(json.toString())
        wr.flush()
        val br = BufferedReader(InputStreamReader(conn.inputStream))

        println("Topic: $toTopic")
        var output: String?
        while (br.readLine().also { output = it } != null) {
            println(output)
        }
    }

    fun sendTrackBattleFoundNotification(userId: String?, battleId: Int?) {
        if (userId != null && battleId != null) {
            sendPushNotification(
                    userId,
                    Topic.TRACK_BATTLE_FOUND,
                    mapOf(Pair(BATTLE_ID_ARG, battleId.toString()))
            )
        }
    }

    fun sendBattleFinishedNotification(userId: String, opponentName: String, scoreString: String, battleResult: BattleResult, bet: Int) {
        sendPushNotification(
                userId,
                Topic.TRACK_BATTLE_FINISHED,
                mapOf(
                        Pair(BATTLE_OPPONENT_NAME_ARG, opponentName),
                        Pair(BATTLE_SCORE_ARG, scoreString),
                        Pair(BATTLE_RESULT_ARG, battleResult.getValue()),
                        Pair(PLUS_COINS_ARG, bet)
                )
        )
    }

    enum class BattleResult(private val value: Int) {
        WON(1), DRAW(2), LOST(3);

        fun getValue() = value
    }

    fun sendJudgeBattleFoundNotification(battleId: Int?) {
        if (battleId != null) {
            sendPushNotification(
                    topic = Topic.JUDGE_BATTLE_FOUND,
                    body = mapOf(Pair(BATTLE_ID_ARG, battleId.toString()))
            )
        }
    }

    enum class Topic(private val value: String) {
        TRACK_BATTLE_FOUND(TRACK_BATTLE_FOUND_TOPIC),
        TRACK_BATTLE_FINISHED(TRACK_BATTLE_FINISHED_TOPIC),
        JUDGE_BATTLE_FOUND(JUDGE_BATTLE_FOUND_TOPIC);

        fun getValue() = value
    }
}