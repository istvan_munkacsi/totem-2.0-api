package com.wepats.totem.rapbattle.model.vote

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class TrackBattleApplicationUserComposeId(
        @Column(name = COLUMN_NAME_USER_ID)
        val userId: String,

        @Column(name = COLUMN_NAME_TRACK_BATTLE_APPLICATION_ID)
        val trackBattleApplicationId: Int
) : Serializable {

    companion object {
        const val COLUMN_NAME_USER_ID = "user_id"
        const val COLUMN_NAME_TRACK_BATTLE_APPLICATION_ID = "track_battle_application_id"
    }
}