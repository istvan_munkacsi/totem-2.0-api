package com.wepats.totem.rapbattle.service.app_version

import com.wepats.totem.rapbattle.model.app_version.AppVersionDto

interface AppVersionServiceContract {

    fun checkVersion(version: Float): AppVersionDto.CompareResponse
}