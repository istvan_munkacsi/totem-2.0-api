package com.wepats.totem.rapbattle.quartz_scheduler.config;

import com.wepats.totem.rapbattle.quartz_scheduler.TotemQuartzScheduler;
import com.wepats.totem.rapbattle.quartz_scheduler.track_battle.NotifyTrackBattleFinishedJob;
import org.quartz.Scheduler;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.quartz.QuartzDataSource;
import org.springframework.boot.autoconfigure.quartz.SchedulerFactoryBeanCustomizer;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import javax.sql.DataSource;

@Configuration
public class QuartzConfiguration {

//    @Autowired
//    private ApplicationContext applicationContext;
//
//    @Bean
//    public SpringBeanJobFactory springBeanJobFactory() {
//        AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();
//
//        jobFactory.setApplicationContext(applicationContext);
//
//        return jobFactory;
//    }
//
//    @Bean
//    public Scheduler schedulerFactoryBean() throws Exception {
//        SchedulerFactoryBean factory = new SchedulerFactoryBean();
//        // this allows to update triggers in DB when updating settings in config file:
//        factory.setOverwriteExistingJobs(true);
//        factory.setDataSource(quartzDataSource());
//
//        JobFactory jobFactory = springBeanJobFactory();
//        factory.setJobFactory(jobFactory);
//
//        Scheduler scheduler = factory.getScheduler();
//        scheduler.setJobFactory(jobFactory);
//
//        scheduler.start();
//        return scheduler;
//    }
//
//    @Bean(name = "notifyTrackBattleFinishedJobDetails")
//    public JobDetailFactoryBean notifyTrackBattleFinishedJobDetails() {
//        JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
//
//        jobDetailFactoryBean.setJobClass(NotifyTrackBattleFinishedJob.class);
//        jobDetailFactoryBean.setName(TotemQuartzScheduler.NOTIFY_BATTLE_FINISHED_JOB_ID);
//        jobDetailFactoryBean.setRequestsRecovery(true);
//        jobDetailFactoryBean.setDurability(true);
//
//        return jobDetailFactoryBean;
//    }

    @Bean
    public SchedulerFactoryBeanCustomizer schedulerFactoryBeanCustomizer() {
        return bean -> bean.setDataSource(quartzDataSource());
    }


    @Bean
    @QuartzDataSource
    public DataSource quartzDataSource() {
        return DataSourceBuilder.create()
                .url("jdbc:mysql://92.119.112.112:3306/totem_quartz?characterEncoding=utf8&serverTimezone=UTC")
                .driverClassName("com.mysql.cj.jdbc.Driver")
                .username("totem")
                .password("1597082sT")
                .build();
    }
}
