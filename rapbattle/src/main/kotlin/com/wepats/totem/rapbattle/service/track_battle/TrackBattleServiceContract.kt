package com.wepats.totem.rapbattle.service.track_battle

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationPostBody
import com.wepats.totem.rapbattle.model.battle.BattleInfo
import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto
import org.springframework.http.ResponseEntity

interface TrackBattleServiceContract {

    fun createOrJoinBattle(trackBattleApplicationPostBody: TrackBattleApplicationPostBody): TrackBattle?
    fun getBattle(battleId: Int): ResponseEntity<TrackBattle>
    fun getBattlesByUserId(userId: String, pageNumber: Int): List<BattleInfo>
}