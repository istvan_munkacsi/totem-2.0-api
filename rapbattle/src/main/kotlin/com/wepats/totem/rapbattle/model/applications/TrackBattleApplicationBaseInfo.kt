package com.wepats.totem.rapbattle.model.applications

data class TrackBattleApplicationBaseInfo(
        val battleId: Int,
        val userId: String,
        val nickname: String,
        val userScore: Int?
) {

    companion object {
        const val PATH = "com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationBaseInfo"
    }
}