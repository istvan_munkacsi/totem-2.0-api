package com.wepats.totem.rapbattle.controller

import com.wepats.totem.rapbattle.model.battle.BattleInfo
import com.wepats.totem.rapbattle.model.user.UserDto
import com.wepats.totem.rapbattle.service.track_battle.TrackBattleServiceContract
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/totem/api/battles")
class BattleController {

    companion object {
        const val PAGE_NUMBER_PARAM = "pageNumber"
    }

    @Autowired
    lateinit var trackBattleService: TrackBattleServiceContract

    @GetMapping("/getByUserId/{${UserController.ID_PARAM}}/{${PAGE_NUMBER_PARAM}}")
    fun getUserBattles(@PathVariable(UserController.ID_PARAM) userId: String, @PathVariable(PAGE_NUMBER_PARAM) pageNumber: Int): ResponseEntity<List<BattleInfo>> {
        val battles = mutableListOf<BattleInfo>()
        battles.addAll(trackBattleService.getBattlesByUserId(userId, pageNumber))

        return ResponseEntity.ok(battles)
    }
}