package com.wepats.totem.rapbattle.repository

import com.wepats.totem.rapbattle.model.track.Track
import com.wepats.totem.rapbattle.model.track.Track.Companion.PATH
import com.wepats.totem.rapbattle.model.track.TrackDto
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.COVER_URL_COLUMN
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.COVER_UUID_COLUMN
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.DURATION_COLUMN
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.ID_COLUMN
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.IS_REMOVED_COLUMN
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.OBJECT_NAME
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.TABLE
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.TITLE_COLUMN
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.TRACK_UUID_COLUMN
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.URL_COLUMN
import com.wepats.totem.rapbattle.model.track.TrackDto.Companion.USER_ID_COLUMN
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional

@Repository
interface TrackRepository : CrudRepository<TrackDto, Int> {

    @Query("SELECT new $PATH(t.id, t.url, t.title, u.nickname, u.id, t.duration, t.coverURL, t.coverUUID, t.trackUUID) FROM $OBJECT_NAME t " +
            "INNER JOIN t.userDto u WHERE t.id = :id")
    fun getById(id: Int): Track?

    @Query("SELECT new $PATH(t.id, t.url, t.title, u.nickname, u.id, t.duration, t.coverURL, t.coverUUID, t.trackUUID) FROM $OBJECT_NAME t " +
            "INNER JOIN t.userDto u WHERE t.id = LAST_INSERT_ID()")
    fun getLastInserted(): Track

    @Query("SELECT new $PATH(t.id, t.url, t.title, u.nickname, u.id, t.duration, t.coverURL, t.coverUUID, t.trackUUID) FROM $OBJECT_NAME t " +
            "INNER JOIN t.userDto u " +
            "WHERE u.id = :id " +
            "AND $IS_REMOVED_COLUMN = 0")
    fun getTracksByUserId(id: String): List<Track>

    @Query("SELECT new $PATH(t.id, t.url, t.title, u.nickname, u.id, t.duration, t.coverURL, t.coverUUID, t.trackUUID) FROM $OBJECT_NAME t " +
            "INNER JOIN t.userDto u " +
            "WHERE t.isInDefaultPlaylist = 1")
    fun getDefaultPlaylist(): List<Track>

    @Query("SELECT 1 " +
            "FROM $OBJECT_NAME track " +
            "INNER JOIN track.trackBattleApplications applications " +
            "INNER JOIN applications.battleDto battle " +
            "WHERE track.$ID_COLUMN = :trackId " +
            "AND (CAST(battle.startedAt AS long) >= :minStartedAt OR CAST(battle.startedAt AS long) = 0)")
    fun getTrackInNotFinishedBattleById(trackId: Int, minStartedAt: Long): Optional<Int>

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO $TABLE($TITLE_COLUMN, $URL_COLUMN, $DURATION_COLUMN, $USER_ID_COLUMN, $TRACK_UUID_COLUMN) " +
            "VALUES(:title, :mediaSource, :duration, :userId, :trackUUID)", nativeQuery = true)
    fun insert(userId: String, title: String, mediaSource: String, duration: Long, trackUUID: String)

    @Query("SELECT COUNT(track.id) FROM $TABLE AS track WHERE $USER_ID_COLUMN = :userId AND track.$IS_REMOVED_COLUMN = 0", nativeQuery = true)
    fun getUserTracksCount(userId: String): Int

    @Modifying
    @Transactional
    @Query(value = "UPDATE $TABLE SET $URL_COLUMN = NULL, $TITLE_COLUMN = NULL, $DURATION_COLUMN = NULL, $COVER_URL_COLUMN = NULL, " +
            "$COVER_UUID_COLUMN = NULL, $TRACK_UUID_COLUMN = NULL, $IS_REMOVED_COLUMN = 1 " +
            "WHERE $ID_COLUMN = :trackId", nativeQuery = true)
    fun updateToNull(trackId: Int): Int

    @Modifying
    @Transactional
    @Query(value = "UPDATE $TABLE SET $TITLE_COLUMN = :title, $COVER_URL_COLUMN = :coverURL, $COVER_UUID_COLUMN = :coverUUID WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun update(id: Int, title: String, coverURL: String?, coverUUID: String?): Int

    @Modifying
    @Transactional
    @Query(value = "UPDATE $TABLE SET $COVER_URL_COLUMN = :coverURL, $COVER_UUID_COLUMN = :coverUUID WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun updateCover(id: Int, coverURL: String?, coverUUID: String?): Int

    @Modifying
    @Transactional
    @Query(value = "UPDATE $TABLE SET $TITLE_COLUMN = :title WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun updateTitle(id: Int, title: String): Int
}