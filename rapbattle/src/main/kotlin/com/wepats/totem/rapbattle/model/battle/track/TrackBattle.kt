package com.wepats.totem.rapbattle.model.battle.track

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplication
import com.wepats.totem.rapbattle.model.battle.BattleInfo

data class TrackBattle(
        val id: Int,
        var startedAt: Long,
        var serverCurrentMillis: Long,
        var applications: MutableList<TrackBattleApplication>,
        var battleStatus: BattleInfo.BattleStatus? = null
) {

    fun toTrackBattleNotification(): TrackBattleNotification {
        return TrackBattleNotification(
                id,
                1000,
                BATTLE_DURATION_MS - (System.currentTimeMillis() - startedAt),
                TrackBattleApplication.toTrackBattleApplicationNotification(applications)
        )
    }

    constructor(id: Int, startedAt: String)
            : this(id,
            startedAt.toLong(),
            System.currentTimeMillis(),
            mutableListOf())

    companion object {
        const val PATH = "com.wepats.totem.rapbattle.model.battle.track.TrackBattle"

        const val BATTLE_DURATION_MS = 1000L * 60 * 60 * 5
    }
}