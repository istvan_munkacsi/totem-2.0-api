package com.wepats.totem.rapbattle.model.applications

data class TrackBattleApplicationNotification(
        val userId: String,
        val nickname: String,
        var userScore: Int
)