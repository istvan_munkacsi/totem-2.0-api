package com.wepats.totem.rapbattle.service.judge_battle

import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.model.vote.TrackBattleAddComment
import com.wepats.totem.rapbattle.model.vote.TrackBattleVote
import com.wepats.totem.rapbattle.model.vote.VoteDetails

interface JudgeBattleServiceContract {

    fun getTrackBattle(userId: String): TrackBattle?
    fun getTrackBattleByIdIfAllowedForUser(userId: String, battleId: Int): TrackBattle?
    fun trackBattleVote(trackBattleVote: TrackBattleVote)
    fun trackBattleAddComment(trackBattleAddComment: TrackBattleAddComment)
    fun getVotesWithDetails(applicationId: Int, pageNumber: Int) : List<VoteDetails>
}