package com.wepats.totem.rapbattle.model.vote

data class TrackBattleVote(
        val userId: String,
        val applicationId: Int
)