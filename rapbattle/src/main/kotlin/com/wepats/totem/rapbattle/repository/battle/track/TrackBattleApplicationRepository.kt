package com.wepats.totem.rapbattle.repository.battle.track

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplication
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationBaseInfo
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto.Companion.BATTLE_ID_COLUMN
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto.Companion.OBJECT_NAME
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto.Companion.TABLE
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto.Companion.TRACK_ID_COLUMN
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationUserAndScore
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface TrackBattleApplicationRepository : CrudRepository<TrackBattleApplicationDto, Int> {

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "INSERT INTO $TABLE($TRACK_ID_COLUMN, $BATTLE_ID_COLUMN) " +
            "VALUES(:trackId, :battleId)")
    fun save(trackId: Int, battleId: Int): Int

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "INSERT INTO $TABLE($BATTLE_ID_COLUMN, $TRACK_ID_COLUMN) " +
            "SELECT LAST_INSERT_ID(), :trackId")
    fun save(trackId: Int): Int

    @Query("SELECT new ${TrackBattleApplication.PATH}" +
            "(" +
            "application.id, user.id, user.nickname, track.id, track.url, track.title, track.duration, track.coverURL, user.photoURL, " +
            "CASE WHEN (votes = null) THEN 0 ELSE SIZE(votes) END" +
            ") " +
            "FROM $OBJECT_NAME application " +
            "INNER JOIN application.trackDto track " +
            "INNER JOIN track.userDto user " +
            "INNER JOIN application.battleDto battle " +
            "LEFT JOIN application.trackBattleVoteDto votes " +
            "WHERE battle.id = :battleId")
    fun getByBattleId(battleId: Int): List<TrackBattleApplication>

    @Query("SELECT new ${TrackBattleApplicationBaseInfo.PATH}" +
            "(" +
            "   battle.id, user.id, user.nickname, CASE WHEN (votes = null) THEN 0 ELSE SIZE(votes) END" +
            ") " +
            "FROM $OBJECT_NAME applications " +
            "INNER JOIN applications.trackDto.userDto user " +
            "INNER JOIN applications.battleDto battle " +
            "LEFT JOIN applications.trackBattleVoteDto votes " +
            "WHERE battle.id IN :battleIds")
    fun getBaseInfoByBattleId(battleIds: List<Int>): List<TrackBattleApplicationBaseInfo>

    @Query("SELECT new ${TrackBattleApplicationUserAndScore.PATH}" +
            "(applications.trackDto.userDto.id, CASE WHEN (votes = null) THEN 0 ELSE SIZE(votes) END) " +
            "FROM $OBJECT_NAME applications " +
            "INNER JOIN applications.trackBattleVoteDto votes " +
            "WHERE applications.battleDto.id = :battleId")
    fun getUserIdAndScore(battleId: Int): List<TrackBattleApplicationUserAndScore>
}