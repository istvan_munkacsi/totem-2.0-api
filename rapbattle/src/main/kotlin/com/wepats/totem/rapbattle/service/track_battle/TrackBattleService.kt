package com.wepats.totem.rapbattle.service.track_battle

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationPostBody
import com.wepats.totem.rapbattle.model.battle.BattleInfo
import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto
import com.wepats.totem.rapbattle.repository.UserRepository
import com.wepats.totem.rapbattle.repository.battle.track.TrackBattleApplicationRepository
import com.wepats.totem.rapbattle.repository.battle.track.TrackBattleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.domain.PageRequest
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class TrackBattleService : TrackBattleServiceContract {

    @Autowired
    lateinit var battleRepo: TrackBattleRepository

    @Autowired
    lateinit var applicationRepo: TrackBattleApplicationRepository

    @Qualifier("userRepository")
    @Autowired
    lateinit var userRepo: UserRepository

    @Transactional
    override fun createOrJoinBattle(trackBattleApplicationPostBody: TrackBattleApplicationPostBody): TrackBattle? {
        val trackId = trackBattleApplicationPostBody.trackId
        val freeBattleId = battleRepo.getFreeBattleId(trackId, PageRequest.of(0, 1))

        userRepo.minusCoins(trackBattleApplicationPostBody.bet, trackId)

        return if (freeBattleId.isEmpty()) {
            battleRepo.insertNew()
            applicationRepo.save(trackId)

            null
        } else {
            applicationRepo.save(trackId, freeBattleId[0])
            battleRepo.updateStartedAt(freeBattleId[0], System.currentTimeMillis().toString())

            battleRepo.getById(freeBattleId[0]).apply {
                applications.addAll(applicationRepo.getByBattleId(freeBattleId[0]))
            }
        }
    }

    override fun getBattle(battleId: Int): ResponseEntity<TrackBattle> {
        return ResponseEntity.ok(battleRepo.getById(battleId).apply {
            val battleApplications = applicationRepo.getByBattleId(battleId)
            applications.addAll(battleApplications)
            battleStatus = TrackBattleDto.getBattleStatus(applications.size, startedAt)
        })
    }

    override fun getBattlesByUserId(userId: String, pageNumber: Int): List<BattleInfo> {
        val battles = battleRepo.getByUserId(userId, PageRequest.of(pageNumber, 10))
        val applications = applicationRepo.getBaseInfoByBattleId(battles.map { it.battleId })

        for (battle in battles) {
            for (application in applications) {
                if (battle.battleId == application.battleId) {
                    battle.info.add(application)
                }
            }
        }

        return battles
    }
}