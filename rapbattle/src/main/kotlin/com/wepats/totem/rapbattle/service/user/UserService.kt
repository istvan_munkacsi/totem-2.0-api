package com.wepats.totem.rapbattle.service.user

import com.wepats.totem.rapbattle.model.user.User
import com.wepats.totem.rapbattle.model.user.UserLinkUpdate
import com.wepats.totem.rapbattle.model.user.UserUpdate
import com.wepats.totem.rapbattle.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService : UserServiceContract {

    @Autowired
    private lateinit var repo: UserRepository

    override fun getOrSave(userId: String): User? {
        return get(userId) ?: insert(User(userId))
    }

    private fun get(userId: String): User? {
        return repo.get(userId)
    }

    override fun getCoins(userId: String): Int {
        return repo.getCoins(userId)
    }

    private fun insert(user: User): User? {
        repo.save(user.id, user.nickname, user.coinsCount, user.photoURL)
        return repo.get(user.id)
    }

    override fun updateCoinsCount(userId: String, newCoinsCount: Int): Int {
        return repo.updateCoinsCount(newCoinsCount, userId)
    }

    override fun plusCoinsCount(userId: String, plusCoinsCount: Int): Int {
        return repo.plusCoinsCount(plusCoinsCount, userId)
    }

    override fun update(userUpdate: UserUpdate): Int {
        return if (userUpdate.photoChanged() && userUpdate.nicknameChanged()) {
            repo.update(userUpdate.nickname!!, userUpdate.photoUUID, userUpdate.photoURL, userUpdate.id)
        } else if (userUpdate.photoChanged()) {
            repo.updatePhotoURL(userUpdate.photoURL, userUpdate.photoUUID, userUpdate.id)
        } else if (userUpdate.nicknameChanged()) {
            repo.update(userUpdate.nickname!!, userUpdate.id)
        } else 0
    }

    override fun updateUserLink(userLinkUpdate: UserLinkUpdate): Int {
        return repo.updateUserLink(userLinkUpdate.link, userLinkUpdate.userId)
    }
}