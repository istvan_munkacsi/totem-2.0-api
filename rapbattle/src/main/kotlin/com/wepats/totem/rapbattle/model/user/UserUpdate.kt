package com.wepats.totem.rapbattle.model.user

data class UserUpdate(
        val id: String,
        val nickname: String?,
        val photoURL: String?,
        val photoRemoved: Boolean,
        val photoUUID: String?
) {
    fun photoChanged() = photoRemoved || photoURL != null
    fun nicknameChanged() = nickname != null
}