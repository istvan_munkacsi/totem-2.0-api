package com.wepats.totem.rapbattle.model.vote

data class TrackBattleAddComment(
        val userId: String,
        val applicationId: Int,
        val rating: Float,
        var comment: String
)