package com.wepats.totem.rapbattle.controller

import com.wepats.totem.rapbattle.model.app_version.AppVersionDto
import com.wepats.totem.rapbattle.service.app_version.AppVersionServiceContract
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/totem/api/version")
class AppVersionController {

    companion object {
        private const val VERSION_NUMBER_PARAM = "versionNumber"
    }

    @Autowired
    private lateinit var service: AppVersionServiceContract

    @GetMapping("/check/{$VERSION_NUMBER_PARAM}")
    fun checkVersion(@PathVariable(VERSION_NUMBER_PARAM) versionNumber: Float): AppVersionDto.CompareResponse {
        return service.checkVersion(versionNumber)
    }
}