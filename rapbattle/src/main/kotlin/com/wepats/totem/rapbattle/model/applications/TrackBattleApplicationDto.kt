package com.wepats.totem.rapbattle.model.applications

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto.Companion.TABLE
import com.wepats.totem.rapbattle.model.battle.track.TrackBattleDto
import com.wepats.totem.rapbattle.model.track.TrackDto
import com.wepats.totem.rapbattle.model.vote.TrackBattleVoteDto
import javax.persistence.*

@Entity
@Table(name = TABLE)
data class TrackBattleApplicationDto(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Int,

        @ManyToOne @JoinColumn(name = TRACK_ID_COLUMN)
        val trackDto: TrackDto,

        @ManyToOne @JoinColumn(name = BATTLE_ID_COLUMN)
        val battleDto: TrackBattleDto,

        @OneToMany(mappedBy = "votedForTrackBattleApplication")
        val trackBattleVoteDto: List<TrackBattleVoteDto>?

) {
    companion object {
        const val TABLE = "track_battle_application"
        const val OBJECT_NAME = "TrackBattleApplicationDto"

        const val ID_COLUMN = "id"
        const val TRACK_ID_COLUMN = "track_id"
        const val BATTLE_ID_COLUMN = "battle_id"
    }
}