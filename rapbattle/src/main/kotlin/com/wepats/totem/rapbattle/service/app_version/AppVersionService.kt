package com.wepats.totem.rapbattle.service.app_version

import com.wepats.totem.rapbattle.model.app_version.AppVersionDto
import com.wepats.totem.rapbattle.repository.AppVersionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AppVersionService : AppVersionServiceContract {

    @Autowired
    private lateinit var repo: AppVersionRepository

    override fun checkVersion(version: Float): AppVersionDto.CompareResponse {
        val allVersions = repo.getAll()
        var response = AppVersionDto.CompareResponse.ACTUAL
        for (v in allVersions) {
            if (v.version > version) {
                if (v.mustUpdate == 1) return AppVersionDto.CompareResponse.MUST_UPDATE
                else response = AppVersionDto.CompareResponse.NOT_ACTUAL
            }
        }
        return response
    }
}