package com.wepats.totem.rapbattle.controller

import com.wepats.totem.rapbattle.firebase.PushNotificationHelper
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationPostBody
import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.quartz_scheduler.TotemQuartzScheduler
import com.wepats.totem.rapbattle.service.track_battle.TrackBattleServiceContract
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.quartz.SchedulerFactoryBean
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/totem/api/trackBattle")
class TrackBattleController {

    companion object {
        const val ID_PARAM = "trackBattleId"
    }

    @Autowired
    private lateinit var trackBattleService: TrackBattleServiceContract

    @Autowired
    lateinit var schedulerFactory: SchedulerFactoryBean

    @PostMapping("/createOrJoinBattle")
    fun getBattle(@RequestBody trackBattleApplicationPostBody: TrackBattleApplicationPostBody): ResponseEntity<Int> {
        val appliedBattle = trackBattleService.createOrJoinBattle(trackBattleApplicationPostBody)

        appliedBattle?.let {
            //send notification for users whose battle is found
            for (application in appliedBattle.applications) {
                val id = application.userId
                PushNotificationHelper.sendTrackBattleFoundNotification(id, appliedBattle.id)
            }

            //send notification for users that are waiting for judgeBattle
            PushNotificationHelper.sendJudgeBattleFoundNotification(it.id)

            //send notification for users whose battle is found when battle is finished
            TotemQuartzScheduler.startNotifyBattleFinishedJob(schedulerFactory, it)
        }

        return ResponseEntity.ok().build()
    }

    @GetMapping("/getBattle/{$ID_PARAM}")
    fun getBattleById(@PathVariable(ID_PARAM) trackBattleId: Int): ResponseEntity<TrackBattle> {
        return trackBattleService.getBattle(trackBattleId)
    }
}