package com.wepats.totem.rapbattle.controller

import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.model.vote.TrackBattleAddComment
import com.wepats.totem.rapbattle.model.vote.TrackBattleVote
import com.wepats.totem.rapbattle.model.vote.VoteDetails
import com.wepats.totem.rapbattle.service.judge_battle.JudgeBattleServiceContract
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/totem/api/judgeBattle")
class JudgeBattleController {

    companion object {
        const val APPLICATION_ID_PARAM = "applicationId"
    }

    @Autowired
    private lateinit var judgeBattleService: JudgeBattleServiceContract

    @GetMapping("/getTrackBattle/{${UserController.ID_PARAM}}")
    fun getTrackBattle(@PathVariable(UserController.ID_PARAM) userId: String): ResponseEntity<TrackBattle> {
        val judgeBattle = judgeBattleService.getTrackBattle(userId)
        return if (judgeBattle != null) {
            ResponseEntity.ok(judgeBattle)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @GetMapping("/getTrackBattle/{${UserController.ID_PARAM}}/{${TrackBattleController.ID_PARAM}}")
    fun getTrackBattleByIdIfAllowedForUser(@PathVariable(UserController.ID_PARAM) userId: String, @PathVariable(TrackBattleController.ID_PARAM) battleId: Int): TrackBattle? {
        return judgeBattleService.getTrackBattleByIdIfAllowedForUser(userId, battleId)
    }

    @PostMapping("/vote/trackBattle/vote")
    fun voteFor(@RequestBody trackBattleVote: TrackBattleVote) {
        judgeBattleService.trackBattleVote(trackBattleVote)
    }

    @PostMapping("/vote/trackBattle/addComment")
    fun addComment(@RequestBody trackBattleAddComment: TrackBattleAddComment) {
        judgeBattleService.trackBattleAddComment(trackBattleAddComment)
    }

    @GetMapping("/vote/trackBattle/getVotesDetails/{$APPLICATION_ID_PARAM}/{${BattleController.PAGE_NUMBER_PARAM}}")
    fun getVotesDetails(@PathVariable(APPLICATION_ID_PARAM) applicationId: Int, @PathVariable(BattleController.PAGE_NUMBER_PARAM) pageNumber: Int): List<VoteDetails> {
        return judgeBattleService.getVotesWithDetails(applicationId, pageNumber)
    }
}