package com.wepats.totem.rapbattle.controller

import com.wepats.totem.rapbattle.model.user.User
import com.wepats.totem.rapbattle.model.user.UserLinkUpdate
import com.wepats.totem.rapbattle.model.user.UserPlusCoins
import com.wepats.totem.rapbattle.model.user.UserUpdate
import com.wepats.totem.rapbattle.service.user.UserServiceContract
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/totem/api/user")
class UserController {

    companion object {
        const val ID_PARAM = "userId"
        const val LINK_PARAM = "link"
    }

    @Autowired
    lateinit var userService: UserServiceContract

    @GetMapping("/{$ID_PARAM}")
    fun getUser(@PathVariable userId: String): ResponseEntity<User> {
        userService.getOrSave(userId)?.let {
            return ResponseEntity.ok(it)
        }
        return ResponseEntity.badRequest().build()
    }

    @GetMapping("/{$ID_PARAM}/coins")
    fun getUserCoins(@PathVariable userId: String): ResponseEntity<Int> {
        return ResponseEntity.ok(userService.getCoins(userId))
    }

    @PutMapping("/{$ID_PARAM}/coins/{newCoinsCount}")
    fun updateUserCoinsCount(@PathVariable userId: String, @PathVariable newCoinsCount: Int): Int {
        return userService.updateCoinsCount(userId, newCoinsCount)
    }

    @PutMapping("/updateUser/plusCoins")
    fun plusUserCoins(@RequestBody userPlusCoins: UserPlusCoins): Int {
        return userService.plusCoinsCount(userPlusCoins.userId, userPlusCoins.plusCoinsCount)
    }

    @PutMapping("/updateUser")
    fun updateNickname(@RequestBody userUpdate: UserUpdate): Int {
        return userService.update(userUpdate)
    }

    @PutMapping("/updateUser/link")
    fun updateUserLink(@RequestBody userLinkUpdate: UserLinkUpdate): Int {
        return userService.updateUserLink(userLinkUpdate)
    }
}