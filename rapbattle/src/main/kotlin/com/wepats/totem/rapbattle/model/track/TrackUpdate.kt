package com.wepats.totem.rapbattle.model.track

data class TrackUpdate(
        val id: Int,
        val title: String?,
        val coverURL: String?,
        val coverRemoved: Boolean,
        val coverUUID: String?
) {
    fun coverChanged() = coverRemoved || coverURL != null
    fun titleChanged() = title != null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TrackUpdate

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}