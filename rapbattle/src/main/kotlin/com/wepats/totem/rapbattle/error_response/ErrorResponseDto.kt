package com.wepats.totem.rapbattle.error_response

data class ErrorResponseDto(
        val errorCode: Int,
        val message: String
)