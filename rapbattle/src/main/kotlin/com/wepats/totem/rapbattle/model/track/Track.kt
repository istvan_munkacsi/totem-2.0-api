package com.wepats.totem.rapbattle.model.track

data class Track(
        val id: Int,
        val mediaSource: String,
        val title: String,
        val artist: String?,
        val userId: String,
        val duration: Long,
        val coverURL: String?,
        val coverUUID: String?,
        val trackUUID: String
) {
    companion object {
        const val PATH = "com.wepats.totem.rapbattle.model.track.Track"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Track

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}