package com.wepats.totem.rapbattle.model.user

data class UserPlusCoins(
        val userId: String,
        val plusCoinsCount: Int
)