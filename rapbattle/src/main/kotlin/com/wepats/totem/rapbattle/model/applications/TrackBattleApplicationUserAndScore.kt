package com.wepats.totem.rapbattle.model.applications

data class TrackBattleApplicationUserAndScore(
        val userId: String,
        val score: Int
) {
    companion object {
        const val PATH = "com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationUserAndScore"
    }
}