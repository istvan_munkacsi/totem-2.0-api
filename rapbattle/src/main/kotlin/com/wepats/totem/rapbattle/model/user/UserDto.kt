package com.wepats.totem.rapbattle.model.user

import com.wepats.totem.rapbattle.model.applications.TrackBattleApplication
import com.wepats.totem.rapbattle.model.applications.TrackBattleApplicationDto
import com.wepats.totem.rapbattle.model.track.TrackDto
import com.wepats.totem.rapbattle.model.user.UserDto.Companion.TABLE
import com.wepats.totem.rapbattle.model.vote.TrackBattleVoteDto
import javax.persistence.*

@Entity
@Table(name = TABLE)
data class UserDto(
        @Column(name = ID_COLUMN) @Id val id: String,
        @Column(name = NICKNAME_COLUMN) val nickname: String,
        @Column(name = COINS_COUNT_COLUMN) val coinsCount: Int,
        @Column(name = PHOTO_URL_COLUMN) val photoURL: String? = null,
        @Column(name = PHOTO_UUID_COLUMN) val photoUUID: String? = null,
        @Column(name = LINK_COLUMN) val link: String? = null,

        @OneToMany(mappedBy = "userDto") val tracks: List<TrackDto>,
        @OneToMany(mappedBy = "userDto") val trackBattlesMyVotes: List<TrackBattleVoteDto>? //,
        //@OneToMany(mappedBy = "userDto") val texts: List<TextDto>
) {
    companion object {
        const val OBJECT_NAME = "UserDto"

        const val TABLE = "user"
        const val ID_COLUMN = "id"
        const val NICKNAME_COLUMN = "nickname"
        const val COINS_COUNT_COLUMN = "coins_count"
        const val PHOTO_URL_COLUMN = "photo_url"
        const val PHOTO_UUID_COLUMN = "photo_uuid"
        const val LINK_COLUMN = "link"
    }
}