package com.wepats.totem.rapbattle.quartz_scheduler

import com.google.gson.Gson
import com.wepats.totem.rapbattle.model.battle.track.TrackBattle
import com.wepats.totem.rapbattle.quartz_scheduler.track_battle.NotifyTrackBattleFinishedJob
import org.quartz.JobBuilder
import org.quartz.JobKey
import org.springframework.scheduling.quartz.SchedulerFactoryBean

object TotemQuartzScheduler {

    private const val NOTIFY_BATTLE_FINISHED_JOB_ID = "notify_battle_finished_job"

    fun startNotifyBattleFinishedJob(schedulerFactory: SchedulerFactoryBean, trackBattle: TrackBattle) {
        val scheduler = schedulerFactory.scheduler
        val job = JobBuilder.newJob().ofType(NotifyTrackBattleFinishedJob::class.java)
                .usingJobData(NotifyTrackBattleFinishedJob.TRACK_BATTLE_NOTIFICATION_KEY, Gson().toJson(trackBattle.toTrackBattleNotification()))
                .storeDurably()
                .requestRecovery()
                .withIdentity(NOTIFY_BATTLE_FINISHED_JOB_ID)
                .build()
        scheduler.addJob(job, true)
        scheduler.triggerJob(JobKey(NOTIFY_BATTLE_FINISHED_JOB_ID))
    }
}