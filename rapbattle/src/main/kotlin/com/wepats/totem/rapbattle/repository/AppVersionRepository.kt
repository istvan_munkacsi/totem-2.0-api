package com.wepats.totem.rapbattle.repository

import com.wepats.totem.rapbattle.model.app_version.AppVersionDto
import com.wepats.totem.rapbattle.model.app_version.AppVersionDto.Companion.TABLE
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface AppVersionRepository : CrudRepository<AppVersionDto, Int> {

    @Query("SELECT * FROM $TABLE", nativeQuery = true)
    fun getAll(): List<AppVersionDto>
}