package com.wepats.totem.rapbattle.repository

import com.wepats.totem.rapbattle.model.track.TrackDto
import com.wepats.totem.rapbattle.model.user.User
import com.wepats.totem.rapbattle.model.user.User.Companion.PATH
import com.wepats.totem.rapbattle.model.user.UserDto
import com.wepats.totem.rapbattle.model.user.UserDto.Companion.COINS_COUNT_COLUMN
import com.wepats.totem.rapbattle.model.user.UserDto.Companion.ID_COLUMN
import com.wepats.totem.rapbattle.model.user.UserDto.Companion.LINK_COLUMN
import com.wepats.totem.rapbattle.model.user.UserDto.Companion.NICKNAME_COLUMN
import com.wepats.totem.rapbattle.model.user.UserDto.Companion.OBJECT_NAME
import com.wepats.totem.rapbattle.model.user.UserDto.Companion.PHOTO_URL_COLUMN
import com.wepats.totem.rapbattle.model.user.UserDto.Companion.PHOTO_UUID_COLUMN
import com.wepats.totem.rapbattle.model.user.UserDto.Companion.TABLE
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Qualifier("userRepository")
@Repository("userRepository")
interface UserRepository : CrudRepository<UserDto, String> {

    @Modifying
    @Transactional
    @Query("UPDATE $TABLE SET $NICKNAME_COLUMN = :nickname, $PHOTO_URL_COLUMN = :photoURL, $PHOTO_UUID_COLUMN = :photoUUID WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun update(nickname: String, photoURL: String?, photoUUID: String?, id: String): Int

    @Modifying
    @Transactional
    @Query("UPDATE $TABLE SET $PHOTO_URL_COLUMN = :photoURL, $PHOTO_UUID_COLUMN = :photoUUID WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun updatePhotoURL(photoURL: String?, photoUUID: String?, id: String): Int

    @Modifying
    @Transactional
    @Query("UPDATE $TABLE SET $NICKNAME_COLUMN = :nickname WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun update(nickname: String, id: String): Int

    @Modifying
    @Transactional
    @Query("UPDATE $TABLE AS user " +
            "INNER JOIN ${TrackDto.TABLE} AS track ON track.${TrackDto.USER_ID_COLUMN} = user.$ID_COLUMN " +
            "SET user.$COINS_COUNT_COLUMN = (user.$COINS_COUNT_COLUMN - :minusBy) " +
            "WHERE track.${TrackDto.ID_COLUMN} = :trackId", nativeQuery = true)
    fun minusCoins(minusBy: Int, trackId: Int): Int

    @Modifying
    @Transactional
    @Query("UPDATE $TABLE SET $COINS_COUNT_COLUMN = :coinsCount WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun updateCoinsCount(coinsCount: Int, id: String): Int

    @Modifying
    @Transactional
    @Query("UPDATE $TABLE SET $COINS_COUNT_COLUMN = $COINS_COUNT_COLUMN + :plusCoinsCount WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun plusCoinsCount(plusCoinsCount: Int, id: String): Int

    @Modifying
    @Transactional
    @Query("UPDATE $TABLE SET $LINK_COLUMN = :link WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun updateUserLink(link: String?, id: String): Int

    @Modifying
    @Transactional
    @Query("UPDATE $TABLE SET $COINS_COUNT_COLUMN = $COINS_COUNT_COLUMN + :bet WHERE $ID_COLUMN = :id", nativeQuery = true)
    fun updateCoinsCountByBet(bet: Int, id: String): Int

    @Modifying
    @Transactional
    @Query("INSERT INTO $TABLE ($ID_COLUMN, $NICKNAME_COLUMN, $COINS_COUNT_COLUMN, $PHOTO_URL_COLUMN) VALUES(:id, :nickname, :coinsCount, :photoURL)", nativeQuery = true)
    fun save(id: String, nickname: String, coinsCount: Int, photoURL: String?): Int

    @Query("SELECT new $PATH(u.id, u.nickname, u.coinsCount, u.photoURL, u.photoUUID, u.link) FROM $OBJECT_NAME u WHERE u.id = :id")
    fun get(id: String): User?

    @Query("SELECT u.coinsCount FROM $OBJECT_NAME u WHERE u.id = :id")
    fun getCoins(id: String): Int
}